#!/usr/bin/python3

import socket

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8000
serversocket.bind((host, port))
serversocket.listen(1)
clientsocket, adress = serversocket.accept()
while True:
    bob = ""
    while bob != "cambio" and bob != "exit":
        bob = clientsocket.recv(1024).decode()
        print(bob)
    if bob == "cambio":
        alice = ""
        while alice != "cambio" and alice != "exit":
            alice = input("> ")
            clientsocket.send(alice.encode())
        if alice == "cambio":
            pass  # Tiene que volver al principio
        elif alice == "exit":
            clientsocket.send("exit".encode())
            break
    elif bob == "exit":
        exit()

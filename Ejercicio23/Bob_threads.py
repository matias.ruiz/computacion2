#!/usr/bin/python3

import socket

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8000
serversocket.connect((host, port))
while True:
    try:
        bob = ""
        while bob != "cambio" and bob != "exit":
            bob = input("> ")
            serversocket.send(bob.encode())
        if bob == "cambio":
            alice = ""
            while alice != "cambio" and alice != "exit":
                alice = serversocket.recv(1024).decode()
                print(alice)
            if alice == "cambio":
                pass # Tiene que volver al principio
            elif alice == "exit":
                exit()
        elif bob == "exit":
            serversocket.send("exit".encode())
            break
    except Exception as e:
        print(e)

#!/usr/bin/python3
from multiprocessing import Pool
import getopt, sys


def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [alist[i * length // wanted_parts: (i + 1) * length // wanted_parts] for i in range(wanted_parts)]

def process_func(numbers_list):
    result = list()
    for number in numbers_list:
        result.append(number**2)
    return result

if __name__ == "__main__":
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], 'p:m:n:')

        process = 1
        num_min = 1
        num_max = 10

        for arg in opts:
            if arg[0] == "-p":
                process = arg[1]
            elif arg[0] == "-m":
                num_min = arg[1]
            elif arg[0] == "-n":
                num_max = arg[1]
            else:
                print("Wrong parameters")
                exit(1)

        list_range = list(range(num_min, num_max))
        splited_list = split_list(list_range, wanted_parts=process)

        pool = Pool(processes=2)

        results = (pool.map(process_func, splited_list))
        print("Pool.map")
        print(results[0])

        results = []

        for i in range(process):
            results.append(pool.apply(process_func, args=(splited_list[i],)))
        print("Pool.apply")
        print(results[0])


    except Exception as e:
        print("Error:", e)

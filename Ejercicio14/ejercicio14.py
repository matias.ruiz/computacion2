#!/usr/bin/python3

import socket
import subprocess
import multiprocessing

def prompt(clientsocket):
    while True:
        data = clientsocket.recv(1024)
        try:
            process = subprocess.Popen(data.decode().split("&"), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            stdout, stderr = process.communicate()
            if stdout.decode() != "":
                clientsocket.send("Ok\n".encode() + stdout)
                print("Command:", data.decode().replace("&", " "), "successfully executed")
            else:
                clientsocket.send("Error\n".encode() + stderr)
                print("Error:", data.decode().replace("&", " "))
        except Exception as error:
            clientsocket.send("Error\n".encode() + str(error).encode())
            print("Error:", data.decode().replace("&", " "))

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8000
serversocket.bind((host, port))
serversocket.listen(5)
while True:
    clientsocket, adress = serversocket.accept()
    child = multiprocessing.Process(target=prompt, args=(clientsocket,))
    child.start()


#!/usr/bin/python3

import socket

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8000
serversocket.connect((host, port))
while True:
    try:
        msg = input("> ").replace(" ", "&").encode()
        serversocket.send(msg)
        print(serversocket.recv(1024).decode())
    except:
        pass
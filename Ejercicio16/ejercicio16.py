from multiprocessing import Process, Queue
import os
import time


def child(num_process, queue):
    time.sleep(num_process)
    queue.put(str(os.getpid())+"\t")

queue = Queue()

process = []
for i in range(10):
    process.append(Process(target=child, args=(i+1, queue,)))
    process[i].start()

for x in process:
    x.join()
while queue:
    print(queue.get())

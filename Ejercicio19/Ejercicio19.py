#!/usr/bin/python3

import multiprocessing, string, getopt, sys, random, time

def process(lock, file, repeat):
    letter = random.choice(string.ascii_letters)
    process_file = open(file, "a")
    lock.acquire()
    for i in range(repeat):
        time.sleep(1)
        process_file.write(letter)
    lock.release()
    process_file.close()

if __name__ == "__main__":
    try:
        lock = multiprocessing.Lock()
        (opts, args) = getopt.getopt(sys.argv[1:], 'n:f:r:')
        num = 0
        file = ""
        repeat = 0
        for arg in opts:
            if arg[0] == "-n":
                num = arg[1]
            elif arg[0] == "-f":
                file = arg[1]
            elif arg[0] == "-r":
                repeat = arg[1]
            else:
                print("Wrong parameters")
                exit(1)
        for i in range(num):
            multiprocessing.Process(target=process, args=(lock, file, repeat,)).start()
    except Exception as e:
        print("Error:", e)

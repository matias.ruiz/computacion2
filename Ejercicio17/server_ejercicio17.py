#!/usr/bin/python3

import socket
import multiprocessing
import getopt
import sys


def prompt(clientsocket):
    while True:
        data = clientsocket.recv(1024)
        try:
            clientsocket.send((str(data.decode())[::-1]).encode())
        except Exception as error:
            clientsocket.send("Error\n".encode() + str(error).encode())
            print("Error:", data.decode())

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'p:', ["port"])

    host = "0.0.0.0"
    port = 8080

    for arg in opts:
        if arg[0] == "-p":
            port = arg[1]
        else:
            print("Wrong parameters")
            exit(-1)

    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.bind((host, port))
    serversocket.listen(5)
    while True:
        clientsocket, adress = serversocket.accept()
        child = multiprocessing.Process(target=prompt, args=(clientsocket,))
        child.start()

except Exception as e:
    print("Wrong parameters:", e)


#!/usr/bin/python3
import getopt
import socket
import sys

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'h:p:', ["host", "port"])

    host = ""
    port = 8080

    for arg in opts:
        if arg[0] == "-h":
            host = arg[1]
        elif arg[0] == "-p":
            port = arg[1]
        else:
            print("Wrong parameters")
            exit(-1)

    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.connect((host, port))
    while True:
        try:
            msg = input("> ").encode()
            if len(msg) != 0:
                serversocket.send(msg)
                print(serversocket.recv(1024).decode())
            else:
                print("Ingrese texto")
        except:
            pass

except Exception as e:
    print("Wrong parameters:", e)


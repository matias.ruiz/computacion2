#!/usr/bin/python3

import getopt
import sys
import os

(opts, args) = getopt.getopt(sys.argv[1:], 't:')

text = None

for arg in opts:
    if arg[0] == "-t":
        text = arg[1]

pipe_file = 'pipe'

#if not os.path.exists(pipe_file):
#    os.mkfifo(pipe_file)


fifo_file = open(pipe_file, "w")
fifo_file.write(text)


#!/usr/bin/python3

import os
import signal
import time

def handler(r, t):
    pass


signal.signal(signal.SIGUSR1, handler)
r, w = os.pipe()
pid = os.fork()


if not pid:
    # Hijo
    signal.pause()
    os.close(w)
    r = os.fdopen(r)
    cadena = r.readline()
    print(str(cadena))
else:
    # Padre
    time.sleep(0.1)
    pipe_file = 'pipe'
    pipein = open(pipe_file, 'r')
    line = pipein.read()
    w = os.fdopen(w, "w")
    w.write(line)
    w.close()
    os.kill(pid, signal.SIGUSR1)

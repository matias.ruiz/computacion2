import os, time, signal


def handler(s, f):
    print("Mensaje del hijo")


signal.signal(signal.SIGUSR1, handler)
pid = os.fork()

if pid == 0:
    while True:
        print("Hijo esperando")
        try:
            signal.pause()
        except:
            print("Adios!!!")
            exit(0)
else:
    for item in range(10):
        try:
            os.kill(pid, signal.SIGUSR1)
            time.sleep(1)
        except:
            pass
    os.kill(pid, signal.SIGTERM)

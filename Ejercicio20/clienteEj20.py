#!/usr/bin/python3

import socket

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8000
serversocket.connect((host, port))
while True:
    try:
        msg = input("> ").encode()
        serversocket.send(msg)
        request = serversocket.recv(1024).decode()
        print(request)
        if request == "Adios":
            break
    except Exception as e:
        print(e)

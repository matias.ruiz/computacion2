#!/usr/bin/python3

import multiprocessing, getopt, sys, socket


def process(lock, clientsocket):
    while True:
        command = clientsocket.recv(1024).decode()
        exit = False
        try:
            if command == "ABRIR":
                clientsocket.send("Ingrese archivo".encode())
                name_file = clientsocket.recv(1024).decode()
                clientsocket.send("Ingrese 'CERRAR', 'AGREGAR' o 'LEER'".encode())
                while True:
                    command = clientsocket.recv(1024).decode()
                    if command == "CERRAR":
                        clientsocket.send("Adios".encode())
                        exit = True
                        break
                    elif command == "AGREGAR":
                        file = open(name_file, "a")
                        clientsocket.send("Ingrese Texto".encode())
                        text = clientsocket.recv(1024).decode()
                        lock.acquire()
                        file.write(text)
                        file.close()
                        lock.release()
                        clientsocket.send("Agregado correctamente".encode())
                    elif command == "LEER":
                        clientsocket.send(open(name_file, "r").read().encode())
                    else:
                        clientsocket.send("Ingrese otro comando".encode())
            else:
                clientsocket.send("Debe abrir un archivo".encode())
        except Exception as error:
            clientsocket.send("Error: ".encode() + str(error).encode())
        if exit:
            break

if __name__ == "__main__":
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], 'p:', ["puerto"])

        port = 8000

        for arg in opts:
            if arg[0] == "-p":
                port = arg[1]
            else:
                print("Wrong parameters")
                exit(1)

        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = ""
        serversocket.bind((host, port))
        serversocket.listen(5)
        lock = multiprocessing.Lock()
        while True:
            clientsocket, adress = serversocket.accept()
            child = multiprocessing.Process(target=process, args=(lock, clientsocket,))
            child.start()

    except Exception as e:
        print("Error:", e)


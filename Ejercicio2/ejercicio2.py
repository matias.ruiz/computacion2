#!/usr/bin/python3.4

import getopt
import sys
try:
	(opts, args) = getopt.getopt(sys.argv[1:], 'i:o:', ["Input file", "Output file"])

	ifile = ""
	ofile = ""
	for arg in opts:
		if arg[0] == "-i":
			ifile = arg[1]
		elif arg[0] == "-o":
			ofile = arg[1]
		else:
			print("Wrong parameters")

	try:
		ifile = open(ifile, "r")
		content = ifile.readlines()
		open(ofile, "w").writelines(content)
	except FileNotFoundError:
		print("File not found")
	except Exception as e:
		print(e)

except Exception as e:
	print("Wrong parameters:", e)


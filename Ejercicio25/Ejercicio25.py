#!/usr/bin/python3

import multiprocessing, getopt, sys


def split_list(alist, wanted_parts=1):
    length = len(alist)
    return [alist[i * length // wanted_parts: (i + 1) * length // wanted_parts] for i in range(wanted_parts)]

def process_func(numbers_list, return_list):
    for number in numbers_list:
        return_list.append(number**2)

if __name__ == "__main__":
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], 'p:m:n:')

        process = 1
        num_min = 1
        num_max = 10

        for arg in opts:
            if arg[0] == "-p":
                process = arg[1]
            elif arg[0] == "-m":
                num_min = arg[1]
            elif arg[0] == "-n":
                num_max = arg[1]
            else:
                print("Wrong parameters")
                exit(1)

        list_range = list(range(num_min, num_max))
        splited_list = split_list(list_range, wanted_parts=process)
        list_process = list()
        manager = multiprocessing.Manager()
        return_list = manager.list()
        for i in range(process):
            list_process.append(multiprocessing.Process(target=process_func, args=(splited_list[i], return_list,)))
        for proceso in list_process:
            proceso.start()
        for proceso in list_process:
            proceso.join()

        print(return_list)

    except Exception as e:
        print("Error:", e)


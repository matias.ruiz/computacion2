#!/usr/bin/python3

import multiprocessing, time, random, getopt, sys


def entrando(consultorios, pacientes, lock, entrar_min, entrar_max):
    while True:
        consultorios.acquire()
        time.sleep(random.randint(entrar_min, entrar_max))
        lock.acquire()
        pacientes.value = pacientes.value + 1
        lock.release()
        print("Paciente entrando. Quedan", consultorios.get_value(), "consultorios disponibles. Paciente:", int(pacientes.value))


def saliendo(consultorios, pacientes, lock, salir_min, salir_max):
    while True:
        time.sleep(random.randint(salir_min, salir_max))
        lock.acquire()
        pacientes.value = pacientes.value - 1
        lock.release()
        consultorios.release()
        print("Paciente atendido. Quedan", consultorios.get_value(), "consultorios disponibles. Paciente:", int(pacientes.value))



if __name__ == "__main__":
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], 'z:a:b:c:d:', ["consultorios", "entrar_min", "entrar_max", "salir_min", "salir_max"])

        num_consultorios = 5
        entrar_min = 1
        entrar_max = 3
        salir_min = 5
        salir_max = 7

        for arg in opts:
            if arg[0] == "-z":
                num_consultorios = arg[1]
            elif arg[0] == "-a":
                entrar_min = arg[1]
            elif arg[0] == "-b":
                entrar_max = arg[1]
            elif arg[0] == "-c":
                salir_min = arg[1]
            elif arg[0] == "-d":
                salir_max = arg[1]
            else:
                print("Wrong parameters")
                exit(1)

        pacientes = multiprocessing.Value('d', 0)

        lock = multiprocessing.Lock()

        consultorios = multiprocessing.Semaphore(num_consultorios)

        multiprocessing.Process(target=entrando, args=(consultorios, pacientes, lock, entrar_min, entrar_max,)).start()
        multiprocessing.Process(target=saliendo, args=(consultorios, pacientes, lock, salir_min, salir_max,)).start()

    except Exception as e:
        print("Error:", e)


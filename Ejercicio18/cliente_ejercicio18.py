#!/usr/bin/python3

import socket
import getopt
import sys
import time

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'h:p:t:')

    host = ""
    port = 37
    protocol = ""

    for arg in opts:
        if arg[0] == "-h":
            host = arg[1]
        elif arg[0] == "-p":
            port = arg[1]
        elif arg[0] == "-t":
            protocol = arg[1]
        else:
            print("Invalid arguments")
            exit(-1)

    if protocol == "tcp":
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.connect((host, port))
        print(serversocket.recv(1024).decode())
    elif protocol == "udp":
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        serversocket.connect((host, port))
        reply, adress = serversocket.recvfrom(1024)
        print(reply.decode())

except Exception as e:
    print(e)

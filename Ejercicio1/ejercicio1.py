#!/usr/bin/python3.4

import getopt
import sys
try:
	(opts, args) = getopt.getopt(sys.argv[1:], 'o:n:m:', ["Operador", "Numero1", "Numero2"])

	operator = ""
	num1 = ""
	num2 = ""

	for arg in opts:
		if arg[0] == "-o":
			operator = arg[1]
		elif arg[0] == "-n":
			num1 = arg[1]
		elif arg[0] == "-m":
			num2 = arg[1]
		else:
			print("Wrong parameters")

	print(num1, operator, num2, "=", eval(num1 + operator + num2))
except Exception as e:
	print("Wrong parameters:", e)
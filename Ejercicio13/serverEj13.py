#!/usr/bin/python3

import socket
import subprocess

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8000
serversocket.bind((host, port))
serversocket.listen(5)
clientsocket, adress = serversocket.accept()
while True:
    data = clientsocket.recv(1024)
    try:
        time = subprocess.Popen(data.decode().split("&"), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = time.communicate()
        if stdout.decode() != "":
            clientsocket.send("Ok\n".encode()+stdout)
            print("Command:", data.decode().replace("&", " "), "successfully executed")
        else:
            clientsocket.send("Error\n".encode()+stderr)
            print("Error:", data.decode().replace("&", " "))
    except Exception as error:
        clientsocket.send("Error\n".encode()+str(error).encode())
        print("Error:", data.decode().replace("&", " "))

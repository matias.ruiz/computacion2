#!/usr/bin/python3

import socket
import getopt
import sys
import time
import os

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8000
serversocket.connect((host, port))
while True:
    msg = input("> ").replace(" ", "&").encode()
    serversocket.send(msg)
    print(serversocket.recv(1024).decode())

#!/usr/bin/python3

import socket
import getopt
import sys
import time

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'p:t:f:')

    port = 8000
    protocol = ""
    ffile = ""
    for arg in opts:
        if arg[0] == "-p":
            port = int(arg[1])
        elif arg[0] == "-t":
            protocol = arg[1]
        elif arg[0] == "-f":
            ffile = arg[1]

    file = open(ffile, "w")
    file.close()

    if protocol == "tcp":
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        host = ""
        serversocket.bind((host, port))
        serversocket.listen(5)
        clientsocket, address = serversocket.accept()
        while True:
            data = clientsocket.recv(1024)
            if "EOF" in data.decode():
                break
            else:
                print("Adress:", address)
                print(data.decode())
                file2 = open(ffile, "r")
                lines2 = file2.readlines()
                file2.close()
                try:
                    file2 = open(ffile, "w")
                    for x2 in lines2:
                        file2.write(x2)
                    file2.write(data.decode())
                    file2.close()
                except:
                    for x3 in lines2:
                        file2.write(x3)
                    file2.close()
            time.sleep(1)
            #clientsocket.send("Recibido".encode())

    elif protocol == "udp":
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        host = ""
        serversocket.bind((host, port))
        while True:
            data, address_and_port = serversocket.recvfrom(1024)
            if "EOF" in data.decode():
                break
            else:
                print("Adress:", address_and_port[0], "Port:", address_and_port[1])
                print(data.decode())
                file2 = open(ffile, "r")
                lines2 = file2.readlines()
                file2.close()
                try:
                    file2 = open(ffile, "w")
                    for x2 in lines2:
                        file2.write(x2)
                    file2.write(data.decode())
                    file2.close()
                except:
                    for x3 in lines2:
                        file2.write(x3)
                    file2.close()
            time.sleep(1)
            #serversocket.sendto("Recibido".encode(), adress_and_port)
    else:
        print("Wrong protocol")
except Exception as e:
    print(e)
print("Finish")
#!/usr/bin/python3

import socket
import getopt
import sys
import time

try:
    (opts, args) = getopt.getopt(sys.argv[1:], 'a:p:t:')

    host = ""
    port = 8000
    protocol = ""

    for arg in opts:
        if arg[0] == "-a":
            host = arg[1]
        elif arg[0] == "-p":
            port = arg[1]
        elif arg[0] == "-t":
            protocol = arg[1]

    if protocol == "tcp":
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.connect((host, port))

        while True:
            for line in sys.stdin:
                serversocket.send(line.encode())
            time.sleep(1)
            #msg = input("Ingrese texto: ").encode()
            #serversocket.send(msg)
            #print(serversocket.recv(1024).decode())


    elif protocol == "udp":
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        serversocket.connect((host, port))
        while True:
            for line in sys.stdin:
                serversocket.sendto(line.encode(), (host, port))
            time.sleep(1)
            #msg = input("Ingrese texto: ").encode()
            #serversocket.sendto(msg, (host, port))
            #reply, adress = serversocket.recvfrom(1024)
            #print("Servidor:", reply)

except Exception as e:
    print(e)

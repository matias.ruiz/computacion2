import os

code = os.fork()

if code == 0:
   # Aca trabaja el hijo
   for i in range(5):
      print("Soy el hijo, PID", os.getpid())
   print("PID", os.getpid(), "terminando")
   os._exit(0)

else:
   # Aca trabaja el padre
   for i in range(2):
      print("Soy el padre, PID", os.getpid(), ", mi hijo es", code)
   os.wait()
   print("Mi proceso hijo, PID", code, ", termino")


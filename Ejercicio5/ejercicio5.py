import os
import getopt
import sys

n = None

(opts, args) = getopt.getopt(sys.argv[1:], 'n:') # Captura de opciones
for opt, arg in opts:
    if opt == "-n":
        n = arg


for i in range(int(n)):
    if os.fork() == 0:
        print("Soy el proceso", os.getpid(), "Mi padre es,", os.getppid())
        break # Rompo bucle al dividir proceso


import time
time.sleep(1) # Tiempo para esperar el prompt

import os
import getopt
import sys
import signal


def handler(s, f):
    print("Soy el PID", os.getpid(), ", recibí la señal", str(s), "de mi padre PID", os.getppid())

signal.signal(signal.SIGUSR2, handler)
n = None

(opts, args) = getopt.getopt(sys.argv[1:], 'p:', ["process"])
for opt, arg in opts:
    if opt == "-p":
        n = arg


ppid = os.getppid()

for i in range(int(n)):
    if ppid == os.getppid():
        pid = os.fork()
        if pid == 0:
            signal.pause()
        else:
            print("Creando proceso:", str(pid))
            os.kill(pid, signal.SIGUSR2)

#!/usr/bin/python3

import os, time, signal


def handler1(s, f):
    # Leer pipe
    print("handler1")
    pass

def handler2(s, f):
    # Escribir en pipe
    print("handler2")
    pass

def handler3(s, f):
    # Escribir en pipe
    print("handler3")
    pass

r, w = os.pipe()
pid = os.fork()


if pid == 0:
    pid2 = os.fork()
    if pid2 == 0:
        # Proceso C
        print("Proceso C")
        signal.signal(signal.SIGUSR1, handler3)
        signal.pause()
        w = os.fdopen(w, "w")
        w.write("Mensaje 2 (PID=" + str(os.getpid()) + ")\n")
        w.close()
        os.kill(int(os.getppid() - 1), signal.SIGUSR2)

    else:
        # Proceso B
        print("Proceso B")
        time.sleep(0.1)
        signal.signal(signal.SIGUSR1, handler2)
        signal.pause()
        w = os.fdopen(w, "w")
        w.write("Mensaje 1 (PID=" + str(os.getpid()) + ")\n")
        w.close()
        os.kill(pid2, signal.SIGUSR1)

else:
    # Proceso A
    print("Proceso A", os.getpid())
    time.sleep(0.3)
    signal.signal(signal.SIGUSR2, handler1)
    os.kill(pid, signal.SIGUSR1)
    signal.pause()
    os.close(w)
    r = os.fdopen(r)
    cadena = r.readline()
    print("A (PID=" + str(os.getpid()) + ") leyendo:\n" + str(cadena))

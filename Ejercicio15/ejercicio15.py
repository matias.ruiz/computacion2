from multiprocessing import Process, Pipe
import os
import time

def child1(pipe):
    while True:
        text = input()
        pipe.send(text)
        time.sleep(1)

def child2(pipe):
    while True:
        print("Leyendo (pid: "+str(os.getpid())+"):", pipe.recv())
        time.sleep(2)

pipe_p1, pipe_p2 = Pipe()

p1 = Process(target=child1, args=(pipe_p1,))
p2 = Process(target=child2, args=(pipe_p2,))

p1.start()
p2.start()

p1.join()

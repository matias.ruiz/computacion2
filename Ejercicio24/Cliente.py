#!/usr/bin/python3
import getopt
import socket
import sys

(opts, args) = getopt.getopt(sys.argv[1:], 'a:p:c:h:')

port = 8000
host = ""
text = ""
hash_function = "sha1"

for arg in opts:
    if arg[0] == "-a":
        host = arg[1]
    elif arg[0] == "-p":
        port = arg[1]
    elif arg[0] == "-c":
        text = arg[1]
    elif arg[0] == "-h":
        hash_function = arg[1]
    else:
        print("Wrong parameters")
        exit(1)

serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = ""
port = 8000
serversocket.connect((host, port))
msg = (text + "&" + hash_function).encode()
serversocket.send(msg)
request = serversocket.recv(1024).decode()
print(request)

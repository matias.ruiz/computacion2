#!/usr/bin/python3

import multiprocessing, getopt, sys, socket, threading, hashlib


def client(clientsocket):
    try:
        client = clientsocket.recv(1024).decode()

        text = client.split("&")[0]
        hash_function = client.split("&")[1]
        result = ""
        if hash_function == "sha1":
            result = hashlib.sha1(text.encode()).hexdigest()
        elif hash_function == "sha224":
            result = hashlib.sha224(text.encode()).hexdigest()
        elif hash_function == "sha256":
            result = hashlib.sha256(text.encode()).hexdigest()
        elif hash_function == "sha384":
            result = hashlib.sha384(text.encode()).hexdigest()
        elif hash_function == "sha512":
            result = hashlib.sha512(text.encode()).hexdigest()
        elif hash_function == "sha3-224":
            result = hashlib.sha3_224(text.encode()).hexdigest()
        elif hash_function == "sha3-256":
            result = hashlib.sha3_256(text.encode()).hexdigest()
        elif hash_function == "sha3-384":
            result = hashlib.sha3_384(text.encode()).hexdigest()
        elif hash_function == "sha3-512":
            result = hashlib.sha3_512(text.encode()).hexdigest()
        else:
            result = "Wrong command"
        clientsocket.send(result.encode())
    except Exception as e:
        clientsocket.send(("Error " + str(e)).encode())


if __name__ == "__main__":
    try:
        (opts, args) = getopt.getopt(sys.argv[1:], 'p:m:t:')

        port = 8000
        host = ""
        multiprocess = True

        for arg in opts:
            if arg[0] == "-p":
                port = arg[1]
            elif arg[0] == "-m":
                multiprocess = True
            elif arg[0] == "-t":
                multiprocess = False
            else:
                print("Wrong parameters")
                exit(1)

        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.bind((host, port))
        serversocket.listen(5)

        if multiprocess:
            while True:
                clientsocket, adress = serversocket.accept()
                child = multiprocessing.Process(target=client, args=(clientsocket,))
                child.start()
        else:
            while True:
                clientsocket, adress = serversocket.accept()
                child = threading.Thread(target=client, args=(clientsocket,))
                child.start()

    except Exception as e:
        print("Error:", e)


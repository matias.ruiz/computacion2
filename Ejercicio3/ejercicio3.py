#!/usr/bin/python3

from subprocess import Popen, PIPE
import getopt
import sys
import time

try:
	(opts, args) = getopt.getopt(sys.argv[1:], 'c:f:l:') # Obtencion de argumentos

	command = ""
	ofile = ""
	lfile = ""

	for arg in opts:
		if arg[0] == "-c":
			command = arg[1]
		elif arg[0] == "-f":
			ofile = arg[1]
		elif arg[0] == "-l":
			lfile = arg[1]
		else:
			print("Wrong parameters")
			exit()

	try:
		process = Popen(command.split(" "), stdout=PIPE, stderr=PIPE) # Ejecucion de comando
		output = process.communicate()
		stdout = output[0] # stdout
		stderr = output[1] # stderr

		output_file = open(ofile, "a") # Abro archivo salida
		log_file = open(lfile, "a") # Abro archivo log

		if stdout.decode() != "": # Verificar si la salida estandar no esta vacia
			output_file.write(stdout.decode() + "\n")
			output_file.close()
			log_file.write(time.strftime("%H:%M:%S") + " : " + time.strftime("%d/%m/%y") + ": Command " + command + " successfully executed" + "\n\n")
		else:
			log_file.write(str(time.strftime("%H:%M:%S") + " : " + time.strftime("%d/%m/%y") + ": Command ") + stderr.decode() + "\n")
		log_file.close()

	except Exception as e: # Manejo de excepciones para comandos mal introducidos
		log_file = open(lfile, "a")
		log_file.write(time.strftime("%H:%M:%S") + " : " + time.strftime("%d/%m/%y") + ": Command: " + str(e) + "\n\n")
		log_file.close()

except Exception as e:
	print("Wrong parameters:", e)
